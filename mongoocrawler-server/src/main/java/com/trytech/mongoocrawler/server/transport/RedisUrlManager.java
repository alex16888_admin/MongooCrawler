package com.trytech.mongoocrawler.server.transport;

import com.trytech.mongoocrawler.server.cache.redis.CrawlerRedisSession;
import com.trytech.mongoocrawler.server.cache.redis.RedisClient;

/**
 * Created by coliza on 2017/4/9.
 */
public class RedisUrlManager extends UrlManager {
    //redis客户端
    private RedisClient client;
    //运行环境
    private CrawlerRedisSession container;

    public RedisUrlManager(CrawlerRedisSession container, RedisClient client){
        this.client = client;
        this.container = container;
    }
    public CrawlerRedisSession getContainer(){
        return container;
    }


    @Override
    public void pushUrl(UrlParserPair urlParserPair){
        client.insertToQueue(container.getRedisKey(),urlParserPair.getUrl());
    }
    @Override
    public void pushUrls(UrlParserPair[] urlParserPairs){
        for(UrlParserPair urlParserPair:urlParserPairs) {
            pushUrl(urlParserPair);
        }
    }
    @Override
    public UrlParserPair fetchUrl() {
        return new UrlParserPair(client.fetchFromQueue(container.getRedisKey()));
    }
}
