package com.trytech.mongoocrawler.server.es;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.plugins.Plugin;

import java.util.Collection;

/**
 * Created by coliza on 2017/6/20.
 */
public class ESTransportClient extends TransportClient {
    public ESTransportClient(Settings settings, Collection<Class<? extends Plugin>> plugins) {
        super(settings, plugins);
    }
}
