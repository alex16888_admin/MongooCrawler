package com.trytech.mongoocrawler.server.xml;

/**
 * Created by coliza on 2017/6/3.
 */
public class BeanAttribute {
    private String name;
    private Object value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
