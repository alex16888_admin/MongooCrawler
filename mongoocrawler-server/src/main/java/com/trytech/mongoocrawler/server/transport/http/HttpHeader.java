package com.trytech.mongoocrawler.server.transport.http;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by coliza on 2017/7/31.
 */
public abstract class HttpHeader
{
    public static enum STATUS_CODE{
        STATUS_200(200,"success"),STATUS_303(303,"redirect"),STATUS_404(404,"not found");;
        private int status;
        private String message;

        STATUS_CODE(int status,String msg){
            this.status = status;
            this.message = msg;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
    protected String httpVersion;//协议版本

    public String getHttpVersion() {
        return httpVersion;
    }

    public void setHttpVersion(String httpVersion) {
        this.httpVersion = httpVersion;
    }

    public Map toMap() {
        Map<String,String> map = new HashMap<String,String>();
        Class cls = getClass();
        Field[ ] fields = cls.getDeclaredFields( );
        for(Field field : fields ){
            field.setAccessible( true );
            try
            {
                // 设置字段可见，即可用get方法获取属性值。
                Object val = field.get(this);
                if(val != null) {
                    map.put(field.getName(), (String)field.get(this));
                }
            }
            catch ( Exception e )
            {
            }
        }

        return map;
    }
}
