package com.trytech.mongoocrawler.server.xml;

import com.trytech.mongoocrawler.server.parser.HtmlParser;
import com.trytech.mongoocrawler.server.pipeline.PipelineProxy;

/**
 * Created by coliza on 2017/6/3.
 */
public class CrawlerXmlConfigBean {
    private String name;

    private String startUrl;

    private int runmode;

    private int fetchtimeout;

    private String urlstoremode;

    private HtmlParser firstparser;
    //存储器
    private PipelineProxy pipeline;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartUrl() {
        return startUrl;
    }

    public void setStartUrl(String startUrl) {
        this.startUrl = startUrl;
    }

    public int getRunmode() {
        return runmode;
    }

    public void setRunmode(int runmode) {
        this.runmode = runmode;
    }

    public int getFetchtimeout() {
        return fetchtimeout;
    }

    public void setFetchtimeout(int fetchtimeout) {
        this.fetchtimeout = fetchtimeout;
    }

    public String getUrlstoremode() {
        return urlstoremode;
    }

    public void setUrlstoremode(String urlstoremode) {
        this.urlstoremode = urlstoremode;
    }

    public HtmlParser getFirstparser() {
        return firstparser;
    }

    public void setFirstparser(HtmlParser firstparser) {
        this.firstparser = firstparser;
    }

    public PipelineProxy getPipeline() {
        return pipeline;
    }

    public void setPipeline(PipelineProxy pipeline) {
        this.pipeline = pipeline;
    }
}
