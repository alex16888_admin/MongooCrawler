package com.trytech.mongoocrawler.server;

import com.trytech.mongoocrawler.server.common.db.CrawlerDataSource;
import com.trytech.mongoocrawler.server.xml.XmlConfigBean;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 爬虫运行环境
 */
public abstract class CrawlerContext {
    protected final static CountDownLatch countDownLatch = new CountDownLatch(1);
    //Session Thread Map
    protected static ConcurrentHashMap<String, Thread> sessionThreadMap = new ConcurrentHashMap<String, Thread>();
    protected static ConcurrentHashMap<String, CrawlerSession> sessionMap = new ConcurrentHashMap<String, CrawlerSession>();
    protected static CrawlerConfig config;
    protected ExecutorService threadPool = Executors.newFixedThreadPool(5);

    public static CrawlerConfig getConfig() {
        return config;
    }

    public static CrawlerContext getCrawlerContext(){
        //获取爬虫配置
        initConfig();
        XmlConfigBean xmlConfigBean = config.getConfigBean();
        CrawlerConfig.CrawlerMode crawlerMode = xmlConfigBean.getCrawlerMode();
        //启动爬虫线程
        if (crawlerMode.equals(CrawlerConfig.CrawlerMode.DISTRIBUTED_MODE)) {
            return new DistributedCrawlerContext();
        }
        return new LocalCrawlerContext();
    }

    private static void initConfig() {
        //加载配置文件
        try {
            config = CrawlerConfig.newInstance(System.getProperty("user.dir") + File.separator + "config.xml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start(){
        //加载session
        doInitSession();
    }

    protected abstract void doInitSession();

    public void destory(){
        //关闭数据源
        if(config != null){
            for(CrawlerDataSource dataSource : XmlConfigBean.getAllDataSources()){
                dataSource.destory();
            }
        }
        threadPool.shutdownNow();
        sessionMap.clear();
        sessionMap = null;
        countDownLatch.countDown();
    }

    public void removeSession(String sessionId){
        sessionMap.remove(sessionId);
    }

    public void registerSession(CrawlerSession session){
        sessionMap.put(session.getSessionId(),session);
    }

    public void checkStatus(){
        for(CrawlerSession session:sessionMap.values()){
            if(session.isPaused()) {
                sessionThreadMap.get(session.getSessionId()).interrupt();
            }
            if(!session.isDestoryed()){
                return;
            }
        }
        destory();
    }

    public CountDownLatch getCountDownLatch(){
        return countDownLatch;
    }
    public void interruptSession(String sessionId){
        sessionThreadMap.get(sessionId).interrupt();
    }

    public CrawlerSession getSession(String sessionId){
        if (StringUtils.isEmpty(sessionId)) {
            return (CrawlerSession) sessionMap.values().toArray()[Math.round((float) Math.random() * 100) % sessionMap.values().size()];
        }
        return sessionMap.get(sessionId);
    }
}
