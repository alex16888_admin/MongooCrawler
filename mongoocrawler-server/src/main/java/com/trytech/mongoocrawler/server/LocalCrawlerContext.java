package com.trytech.mongoocrawler.server;

import com.trytech.mongoocrawler.server.transport.UrlParserPair;
import com.trytech.mongoocrawler.server.xml.CacheXmlConfigBean;
import com.trytech.mongoocrawler.server.xml.CrawlerXmlConfigBean;

/**
 * Created by coliza on 2018/4/30.
 */
public class LocalCrawlerContext extends CrawlerContext {
    @Override
    protected void doInitSession() {
        //获取url存储在哪里
        for (CrawlerXmlConfigBean crawlerXmlConfigBean : config.getConfigBean().getCrawlers().values()) {
            LocalCrawlerSession initSession = null;
            CacheXmlConfigBean cacheXmlConfigBean = config.getConfigBean().getCacheXmlConfigBean();
            String startUrl = crawlerXmlConfigBean.getStartUrl();
            //注册初始session
            UrlParserPair urlParserPair = new UrlParserPair(startUrl, crawlerXmlConfigBean.getFirstparser());
            initSession = new LocalCrawlerSession(this, crawlerXmlConfigBean, new UrlParserPair[]{urlParserPair});
            crawlerXmlConfigBean.getPipeline().setCrawlerSession(initSession);
            registerSession(initSession);
        }

        for (CrawlerSession session : sessionMap.values()) {
            Thread thread = new Thread((LocalCrawlerSession) session);
            sessionThreadMap.put(session.getSessionId(), thread);
            thread.start();
        }

        try {
            countDownLatch.await();
        } catch (InterruptedException e) {

        }
        System.out.println("爬取结束");
    }
}
