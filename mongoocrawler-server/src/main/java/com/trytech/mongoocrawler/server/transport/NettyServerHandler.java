package com.trytech.mongoocrawler.server.transport;

import com.trytech.mongoocrawler.common.protocol.*;
import com.trytech.mongoocrawler.common.protocol.adapter.CommandAdapter;
import com.trytech.mongoocrawler.common.protocol.adapter.DataAdapter;
import com.trytech.mongoocrawler.server.CrawlerContext;
import com.trytech.mongoocrawler.server.DistributedCrawlerSession;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.net.MalformedURLException;

/**
 * Created by coliza on 2018/5/1.
 */
public class NettyServerHandler extends SimpleChannelInboundHandler<CrawlerTransferProtocol> {

    private CrawlerContext crawlerContext;

    public NettyServerHandler(CrawlerContext crawlerContext){
        this.crawlerContext = crawlerContext;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, CrawlerTransferProtocol transferProtocol) throws Exception {
        System.out.println("SimpleServerHandler.channelRead");
        AbstractProtocol protocol = null;
        Object obj = transferProtocol.getContent();
        if (obj instanceof CommandProtocol) {
            protocol = ((CommandProtocol) obj).resolve(new CommandAdapter<UrlProtocol>() {
                @Override
                public UrlProtocol handleProtocol(AbstractProtocol abstractProtocol, CommandProtocol.Command command) {
                    if(command.equalsTo(CommandProtocol.Command.GET_URL)){
                        //获取session
                        DistributedCrawlerSession crawlerSession = null;
                        crawlerSession = (DistributedCrawlerSession) crawlerContext.getSession(abstractProtocol.getSessionId());

                        UrlParserPair urlParserPair = crawlerSession.fetchUrl();
                        //从url池中获取url
                        UrlProtocol urlProtocol = new UrlProtocol();
                        urlProtocol.setTraceId("001");
                        urlProtocol.setUrl(urlParserPair.getUrl());
                        return urlProtocol;
                    }else if(command.equalsTo(CommandProtocol.Command.HEARTBEAT)){
                        //如果是心跳
                        return null;
                    }else{
                        //如果是结束命令
                        return null;
                    }
                }
            });
        }
        if (obj instanceof HtmltextProtocol) {
            protocol = ((HtmltextProtocol) obj).resolve(new DataAdapter<CommandProtocol>() {
                @Override
                public CommandProtocol handleProtocol(AbstractProtocol protocol, String data) throws MalformedURLException {
                    //获取url对应的session
                    DistributedCrawlerSession crawlerSession = (DistributedCrawlerSession)crawlerContext.getSession(protocol.getSessionId());
                    //解析爬取数据
                    crawlerSession.parse(((HtmltextProtocol) protocol));
                    //通知客户端可以继续发送命令
                    CommandProtocol commandProtocol = new CommandProtocol();
                    commandProtocol.setCommand(CommandProtocol.Command.CONTINUE);
                    commandProtocol.setTraceId(protocol.getTraceId());
                    return commandProtocol;
                }
            });
        }
        // 向客户端发送消息
        // 在当前场景下，发送的数据必须转换成ByteBuf数组
        transferProtocol = new CrawlerTransferProtocol();
        transferProtocol.setContent(protocol);
        transferProtocol.setType(protocol.getType().val());
        transferProtocol.setCls(protocol.getClass());
        ctx.writeAndFlush(transferProtocol).sync();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        // 当出现异常就关闭连接
        cause.printStackTrace();
        ctx.close();
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }
}
