package com.trytech.mongoocrawler.server.transport.tcp;

import com.trytech.mongoocrawler.common.protocol.ProtocolDecoder;
import com.trytech.mongoocrawler.common.protocol.ProtocolEncoder;
import com.trytech.mongoocrawler.server.CrawlerConfig;
import com.trytech.mongoocrawler.server.CrawlerContext;
import com.trytech.mongoocrawler.server.transport.NettyServerHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * Created by coliza on 2017/10/7.
 */
public class NettyTcpServer {
    protected CrawlerConfig config;
    private CrawlerContext crawlerContext;

    public NettyTcpServer(CrawlerContext context, CrawlerConfig config){
        this.config = config;
        this.crawlerContext = context;
    }

    public void start() {
        //启动服务器端
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast("decoder", new ProtocolDecoder());
                            ch.pipeline().addLast("encoder", new ProtocolEncoder());
                            ch.pipeline().addLast(new NettyServerHandler(crawlerContext));
                        }
                    });

            ChannelFuture f = b.bind(8889).sync();
            f.channel().closeFuture().sync();
        } catch (Exception e) {

        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }

        crawlerContext.start();
    }
}
