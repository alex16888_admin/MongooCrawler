package com.trytech.mongoocrawler.server;

import com.trytech.mongoocrawler.server.parser.HtmlParser;
import com.trytech.mongoocrawler.server.transport.UrlParserPair;
import com.trytech.mongoocrawler.server.transport.tcp.NettyTcpServer;
import com.trytech.mongoocrawler.server.xml.CrawlerXmlConfigBean;

/**
 * Created by coliza on 2018/4/30.
 */
public class DistributedCrawlerContext extends CrawlerContext {
    @Override
    protected void doInitSession() {
        //根据配置初始化不同的session
        for (CrawlerXmlConfigBean crawlerXmlConfigBean : config.getConfigBean().getCrawlers().values()) {
            DistributedCrawlerSession initSession = null;
            //注册初始session
            initSession = new DistributedCrawlerSession(this, crawlerXmlConfigBean);
            crawlerXmlConfigBean.getPipeline().setCrawlerSession(initSession);
            String startUrl = crawlerXmlConfigBean.getStartUrl();
            HtmlParser htmlParser = crawlerXmlConfigBean.getFirstparser();
            initSession.pushUrl(new UrlParserPair(startUrl, htmlParser));
            registerSession(initSession);
        }

        //启动server
        NettyTcpServer server = new NettyTcpServer(this, config);
        server.start();
    }
}
