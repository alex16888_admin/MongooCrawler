package com.trytech.mongoocrawler.common.protocol;

import com.trytech.mongoocrawler.common.protocol.adapter.ProtocolAdapter;

import java.io.Serializable;
import java.net.MalformedURLException;

/**
 * Created by coliza on 2018/3/24.
 */
public abstract class AbstractProtocol<S extends AbstractProtocol, T extends ProtocolAdapter> implements Serializable{
    /***
     * 跟踪ID
     */
    protected String traceId;

    protected String sessionId;

    public static ProtocolType getType(int type){
        if(type == 0){
            return ProtocolType.COMMAND;
        }
        if(type == 1){
            return ProtocolType.URL;
        }
        if(type == 2){
            return ProtocolType.DATA;
        }
        return null;
    }

    public abstract ProtocolType getType();

    public abstract S resolve(T t) throws MalformedURLException;

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public abstract String toJSONString();

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
