package com.trytech.mongoocrawler.common.protocol;

import com.alibaba.fastjson.JSONObject;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年03月28日
 */
public class ProtocolDecoder extends ByteToMessageDecoder implements Protocol {

    private boolean isContent = false;

    private CrawlerTransferProtocol protocol = new CrawlerTransferProtocol();

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        int ibyte = byteBuf.getByte(byteBuf.readerIndex());

        if (byteBuf.readableBytes() > 0 && (isContent || (!isContent && ibyte == MSG_BEGIN))) {
            if (!isContent) {
                byteBuf.readByte();
                byte typeByte = byteBuf.readByte();
                protocol.setType(typeByte);

                int classNameLen = byteBuf.readInt();
                byte[] classNameByteArr = new byte[classNameLen];
                byteBuf = byteBuf.readBytes(classNameByteArr);
                String className = new String(classNameByteArr);
                protocol.setCls(Class.forName(className));
            }
            byteBuf.markReaderIndex();

            int contentLen = byteBuf.readInt();
            if (contentLen <= 0) {
                channelHandlerContext.close();
            }else {
                if (byteBuf.readableBytes() < contentLen) {
                    isContent = true;
                    byteBuf.resetReaderIndex();
                    return;
                }
                //读取正文
                byte[] content = new byte[contentLen];
                byteBuf = byteBuf.readBytes(content);
                String contentStr = new String(content);
                protocol.setContent(JSONObject.parseObject(contentStr, protocol.getCls()));

                if (protocol != null) {
                    list.add(protocol);
                }

                ibyte = byteBuf.readByte();
                if (isContent && ibyte == MSG_END) {
                    isContent = false;
                }
            }
        }
    }
}
