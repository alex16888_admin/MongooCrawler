package com.trytech.mongoocrawler.common.protocol.adapter;

import com.trytech.mongoocrawler.common.protocol.AbstractProtocol;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年04月07日
 */
public interface UrlAdapter<T> extends ProtocolAdapter<String> {
    @Override
    T handleProtocol(AbstractProtocol protocol, String url);
}
