package com.trytech.mongoocrawler.common.protocol.adapter;

import com.trytech.mongoocrawler.common.protocol.AbstractProtocol;
import com.trytech.mongoocrawler.common.protocol.CommandProtocol;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年04月06日
 */
public interface CommandAdapter<T extends AbstractProtocol> extends ProtocolAdapter<CommandProtocol.Command>{

    @Override
    T handleProtocol(AbstractProtocol abstractProtocol, CommandProtocol.Command command);
}
