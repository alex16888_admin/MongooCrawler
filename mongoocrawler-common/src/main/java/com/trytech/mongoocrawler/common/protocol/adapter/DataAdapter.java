package com.trytech.mongoocrawler.common.protocol.adapter;

import com.trytech.mongoocrawler.common.protocol.AbstractProtocol;

import java.net.MalformedURLException;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年04月07日
 */
public interface DataAdapter<T> extends ProtocolAdapter<String> {
    @Override
    T handleProtocol(AbstractProtocol abstractProtocol, String data) throws MalformedURLException;
}
