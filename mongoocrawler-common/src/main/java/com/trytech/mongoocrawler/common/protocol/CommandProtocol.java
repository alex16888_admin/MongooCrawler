package com.trytech.mongoocrawler.common.protocol;

import com.alibaba.fastjson.JSONObject;
import com.trytech.mongoocrawler.common.protocol.adapter.CommandAdapter;

/**
 * Created by coliza on 2018/3/24.
 */
public class CommandProtocol<T extends AbstractProtocol> extends AbstractProtocol<T, CommandAdapter<T>> {

    @Override
    public ProtocolType getType() {
        return ProtocolType.COMMAND;
    }

    private Command command;

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    @Override
    public T resolve(CommandAdapter adapter) {
        Command command = this.getCommand();
        return (T)adapter.handleProtocol(this, command);
    }

    @Override
    public String toJSONString() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", getType().val());
        jsonObject.put("command", getCommand().getValue());
        return jsonObject.toJSONString();
    }

    public enum Command {
        GET_URL((byte) 1), HEARTBEAT((byte) 2), CONTINUE((byte) 3), OVER((byte) 4);
        private byte value;

        Command(byte value) {
            this.value = value;
        }

        public byte getValue() {
            return value;
        }

        public boolean equalsTo(Command command){
            return getValue() == command.getValue();
        }
    }


}
