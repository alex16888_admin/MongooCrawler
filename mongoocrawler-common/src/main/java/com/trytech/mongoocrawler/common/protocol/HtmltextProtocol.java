package com.trytech.mongoocrawler.common.protocol;

import com.alibaba.fastjson.JSONObject;
import com.trytech.mongoocrawler.common.protocol.adapter.DataAdapter;
import lombok.Data;

import java.net.MalformedURLException;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年04月06日
 */
@Data
public class HtmltextProtocol<T extends AbstractProtocol> extends AbstractProtocol<T, DataAdapter> {

    private String traceId;
    private String url;
    private String data;

    @Override
    public ProtocolType getType() {
        return ProtocolType.DATA;
    }

    @Override
    public T resolve(DataAdapter dataAdapter) throws MalformedURLException {
        return (T)dataAdapter.handleProtocol(this, data);
    }

    @Override
    public String toJSONString() {
        data = data.replaceAll("\"","'");
        return JSONObject.toJSONString(this);
    }
}
