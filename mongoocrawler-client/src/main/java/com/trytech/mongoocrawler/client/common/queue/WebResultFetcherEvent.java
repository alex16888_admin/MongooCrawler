package com.trytech.mongoocrawler.client.common.queue;

import com.trytech.mongoocrawler.client.transport.http.WebResult;

/**
 *
 * @author jiangtao
 * @date 2017-01-25
 */
public class WebResultFetcherEvent extends FetcherEvent<WebResult> {
    @Override
    public WebResult getData() {
        return this.data;
    }

    @Override
    public void setData(WebResult data) {
        this.data = data;
    }
}
