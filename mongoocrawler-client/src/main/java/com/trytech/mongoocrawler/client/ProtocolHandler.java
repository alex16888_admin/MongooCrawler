package com.trytech.mongoocrawler.client;

import com.trytech.mongoocrawler.common.protocol.AbstractProtocol;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年04月10日
 */
public abstract class ProtocolHandler<S extends AbstractProtocol> {

    public <T extends AbstractProtocol> AbstractProtocol handle(ClientFilterChain clientFilterChain, T t) throws Exception {
        AbstractProtocol protocol = null;
        if (checkProtocol(t)) {
            protocol = doHandle((S) t);
            if (protocol != null) {
                clientFilterChain.resetIndex();
                return protocol;
            }
        }
        return clientFilterChain.doFilter(t);
    }

    public abstract <T extends AbstractProtocol> AbstractProtocol doHandle(S s);

    protected abstract <T extends AbstractProtocol> boolean checkProtocol(T t);
}
