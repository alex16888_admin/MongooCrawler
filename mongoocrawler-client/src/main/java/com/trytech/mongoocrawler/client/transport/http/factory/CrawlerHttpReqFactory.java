package com.trytech.mongoocrawler.client.transport.http.factory;

import com.trytech.mongoocrawler.client.parser.HtmlParser;
import com.trytech.mongoocrawler.client.transport.http.CrawlerHttpRequest;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by hp on 2017-1-24.
 */
public class CrawlerHttpReqFactory {

    public static CrawlerHttpRequest getRequest(URL url, HtmlParser parser) throws MalformedURLException {
        CrawlerHttpRequest crawlerHttpRequest = new CrawlerHttpRequest(url,null,parser);
        return crawlerHttpRequest;
    }
}
